import re

#Please make sure file is in the appropriate directory. Name file here:
file = 'my_new_data.txt'

#Import and transform the requested file
orgfile = open('Path' + file).read().splitlines()
strconvfile = str(orgfile)
strpconvfile = strconvfile.strip()
trfm1 = re.sub(r'\'', r'_', strpconvfile)
trfm2 = re.sub(r',', r'\n', trfm1)
trfm3 = re.sub(r' ', r'', trfm2)
trfm4 = re.sub(r'\[', r'', trfm3)
trfm5 = re.sub(r'\]', r'', trfm4)
trfm6 = re.sub(r'_\w\w\d\d_', r'_', trfm5)
trfm7 = re.sub(r'_\w\w\d\d\w_', r'_', trfm6)
trfm8 = re.sub(r'_\d\d\w\w_', r'_', trfm7)
trfm9 = re.sub(r'_\d\d\w\d\d_', r'_', trfm8)
trfm10 = re.sub(r'_\d\d_', r'_', trfm9)
trfm11 = re.sub(r'_\d\d\w_', r'_', trfm10)
trfm12 = re.sub(r'_\d\w\w_', r'_', trfm11)

#Create a new file to write the transform to
newtrmfile = open('Path2','w')
newtrmfile.write(str(trfm12))
newtrmfile.close()

#Read in the respective files master set tokens and transformed file from prior logic
mastertoken = open('Path3').read().splitlines()
nxtdatafile = open('Path4').read().splitlines()

#transform the read in files to ensure token parsing
nxtdatafile1 = str(nxtdatafile)
nxtdatafile2 = nxtdatafile1.strip()
nxtdatafile3 = nxtdatafile2.split('_')

#create sets to remove duplicates, increase processing efficiency.
set_of_input_tokens = set(nxtdatafile3)
set_of_master_tokens = set(mastertoken)
output = (set_of_input_tokens - set_of_master_tokens)

#sort to create a readable token list
sorttokens = sorted(output)

#Generate token output report. Consists of Unknown Tokens and Data Length
tokenoutput_report = open('Path5','w')
header = '\n\n##BELOW IS THE UNKNOWN TOKENS:##\n\n'
tokenoutput_report.write(header)

#Transform captured tokens. This cleans the data to increase readability
val = str(sorttokens)
val2 = re.sub(r' ', r'', val)
val3 = re.sub(r'"', r'', val2)
val4 = re.sub(r'\'', r'', val3)
val5 = re.sub(r'\[', r'', val4)
val7 = re.sub(r'\]', r'', val5)
val8 = re.sub(r'(\d),',r'\1\n', val7)
val9 = re.sub(r'(\w),',r'\1\n', val8)
val10 = re.sub(r',', r'', val9)

#val8 = val7.replace(',', '\n')
#val9 = re.sub(r"\r\n", "test", val8)
#val9 = val8.replace('\r\n', '')
#val9 = re.sub(r'(\r)\n', r'\1sdf', val8)
tokenoutput_report.write(val10)

#Now start the process of generating length offenders
header2 = '\n\n##BELOW IS THE FAIL OF 25 LENGTH:##\n\n'
tokenoutput_report.write(header2)

#Data length logic
datalength = nxtdatafile
for i in datalength:
    result = (i + ' !ERROR!') if len(i) > 27 else '' #(i + ' !_PASS!!')
    outputs = (str(result))
    data_length_outputs = outputs.replace('R!', 'R!\n')
    for z in data_length_outputs:
        tokenoutput_report.write(z)
tokenoutput_report.close()